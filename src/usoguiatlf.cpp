#include "guiatlf.h"
#include <iostream>
#include <fstream>
#include <string>


int main(int argc, char * argv[]) {
	Guia_Tlf guia;
	Guia_Tlf auxGuia;
	std::ifstream fin(argv[1]);
	fin>>guia;

	for(int i=2;i<argc;i++) {
		std::ifstream auxfin(argv[i]);
		auxfin>>auxGuia;
		guia+=auxGuia;
	}
	std::cout<<"Contactos:\n"<<guia<<endl;

	string input;
	std::cout<<"Escriba el nombre del contacto que queremos borrar: ";
	std::cin>>input;
	guia.borrar(input);

	string aux;

	std::cout<<"Vamos a buscar el telefono del contacto introducido: ";
	std::cin>>input;
	aux = guia.gettelefono(input);
	std::cout<<aux<<endl;
	std::cout<<"Parece tambien ser erróneo, vamos a cambiarlo por otro nuevo, introduzcalo: ";
	std::cin>>aux;
	guia.cambiartelefono(input,aux);

	std::cout<<"Vamos a cambiarle el nombre al contacto introducido: ";
	std::cin>>input;
	std::cout<<"Introduzca el nuevo nombre: ";
	std::cin>>aux;
	guia.cambiarnombre(input,aux);

	std::cout<<"Vamos a comparar la guia actual con otra introducida, introduzca la ruta de archivo: ";
	std::cin>>input;
	std::ifstream newfin(input);
	newfin>>auxGuia;

	guia = guia-auxGuia;
	std::cout<<"Este es el resultado\nContactos:\n"<<guia;

	std::cout<<"Vamos a guardarla en un archivo, introduzca la ruta del archivo: ";
	std::cin>>input;
	std::cout<<guia<<endl;
	std::ofstream fout(input);
	fout<<guia;

	std::cout<<"Vamos a limpiar la lista de contactos y comprobar que se limpia\n";
	guia.clear();
	std::cout<<"Contactos:\n"<<guia<<endl;

	return 0;
}
