# ed_p4
Práctica número 4 de la asginatura de Estructura de Datos de la UGR

**Métodos añadidos a la clase diccionario:**
- `getClave` Devuelve una clave dada como argumento
- `operator+` Realiza la unión de dos diccionarios
- `erase` Elimina un significado de una palabra