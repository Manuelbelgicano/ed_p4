/**
 * @filename usodiccionario.cpp
 * @author Manuel Gachs Ballegeer
 * 	   Gonzalo Moreno Soto
 */

#include<fstream>
#include<iostream>
#include<string>
#include"diccionario.h"
/**
 * @brief Sobrecarga del operador de entrada
 * @param is Miembro de la clase istream
 * @param d Diccionario que se que se quiere insertar
 * @return El miembro istream
 */
std::istream& operator>>(std::istream &is,diccionario<std::string,std::string> &d);
/**
 * @brief Sobrecarga del operador de salida
 * @param is Miembro de la clase ostream
 * @param d Diccionario que se que se quiere mostrar
 * @return El miembro ostream
 */
std::ostream& operator<<(std::ostream &os,const diccionario<std::string,std::string> &d);
/**
 * @brief Sobrecarga del operador de salida
 * @param os Miembro de la clase ostream
 * @param l Lista a imprimir
 * @return El miembro ostream
 */
std::ostream& operator<<(std::ostream &os,std::list<std::string> &l);
/**
 * @brief Genera un diccionario a partir de un fichero de texto
 * @param filename Nombre del archivo
 * @return El diccionario generado
 */
diccionario<std::string,std::string> fromFile(const char filename[]);
/**
 * @brief Guarda un diccionario en un fichero
 * @param d Diccionario a guardar
 * @param filename Fichero donde se guarda
 * @return @retval true Si la operación se realiza con éxito
 * 	   @retval false En caso contrario
 */
bool toFile(const diccionario<std::string,std::string> &d,const char filename[]);

int main(int argc,const char* argv[]) {
	//Comprobación de argumentos
	if (argc!=3) {
		std::cout<<"Error en el número de argumentos\n";
		return 1;
	}
	
	//Palabra a añadir
	std::string clave = "comedor";
	std::list<std::string> info = {"1. adj. Que come mucho"};
	
	//Significado a añadir
	std::string nuevoSignificado = "2. m. Pieza destinada en las casas para comer";

	//Generación de diccionario
	diccionario<std::string,std::string> d;
	d = fromFile(argv[1]);
	std::cout<<"\nEl diccionario tiene "<<d.size()<<" palabras\n";

	//Insertar palabra en el diccionario
	std::cout<<"\nInsertando la palabra "<<clave;
	std::cout<<" con significado:\n"<<info;
	d.insertar(clave,info);
	std::cout<<"Añadiendole un nuevo significado:\n";
	std::cout<<nuevoSignificado<<std::endl;
	d.addSignificadoPalabra(clave,nuevoSignificado);
	std::cout<<"\nEl diccionario tiene "<<d.size()<<" palabras\n";
	
	//Añadir un nuevo diccionario
	std::cout<<"\nAñadiendo segundo diccionario\n";
	diccionario<std::string,std::string> d2 = fromFile(argv[2]);
	std::cout<<"\nEl segundo diccionario tiene "<<d2.size()<<" palabras\n";
	
	//Unión de diccionarios
	std::cout<<"\nUniendo ambos diccionarios\n";
	d = d + d2;
	std::cout<<"\nEl nuevo diccionario tiene "<<d.size()<<" palabras\n";
	
	//Eliminación de significados
	std::string clave_eliminar = d.getClave(1);
	std::list<std::string> info_eliminar = d.getInfoAsoc(clave_eliminar);
	info_eliminar.resize(1);
	std::string significado_eliminar = info_eliminar.front();
	std::cout<<"\nEliminando el significado: ";
	std::cout<<significado_eliminar<<std::endl;
	std::cout<<"de la palabra "<<clave_eliminar<<std::endl;
	d.erase(clave_eliminar,significado_eliminar);

	//Guardado del diccionario
	std::cout<<"\nGuardando diccionario\n";
	toFile(d,"test/salida_dic_test.txt");

	return 0;
}

std::istream& operator>>(std::istream &is,diccionario<std::string,std::string> &d) {
	unsigned int wordcount = 0;
	unsigned int infocount = 0;
	std::string key;
	std::string meaning;
	std::list<std::string> info;

	is>>wordcount;
	for (size_t i=0;i<wordcount;i++) {
		is>>infocount>>key;
		std::cout<<"Leyendo palabra "<<key<<std::endl;
		is.ignore();
		for (size_t j=0;j<infocount;j++) {
			getline(is,meaning);
			info.push_back(meaning);
		}
		info.sort();
		d.insertar(key,info);
		info.resize(0);
		std::cout<<"Palabra añadida\n";
	}
	return is;
}

std::ostream& operator<<(std::ostream &os,const diccionario<std::string,std::string> &d) {
	diccionario<std::string,std::string>::const_iterator it;
	for (it=d.cbegin();it!=d.cend();it++) {
		os<<(*it).clave<<std::endl;
		std::list<std::string> info = (*it).info_asoci;
		os<<info;
	}
	return os;
}

std::ostream& operator<<(std::ostream &os,std::list<std::string> &l) {
	std::list<std::string>::iterator it;
	for (it=l.begin();it!=l.end();it++)
		os<<*it<<std::endl;
	return os;
}

diccionario<std::string,std::string> fromFile(const char filename[]) {
	std::ifstream fin(filename);
	diccionario<std::string,std::string> d;

	if (!fin)
		std::cout<<"Error en la apertura del archivo "<<filename<<std::endl;
	else {
		fin>>d;
		if (!fin)
			std::cout<<"Error en la creación del diccionario\n";
	}
	fin.close();
	return d;
}

bool toFile(const diccionario<std::string,std::string> &d,const char filename[]) {
	bool aux = false;
	std::ofstream fout(filename);

	if (!fout)
		std::cout<<"Error en la apertura del archivo "<<filename<<std::endl;
	else {
		fout<<d;
		if (fout)
			aux = true;
		else
			std::cout<<"Error en el guardado del diccionario\n";
	}
	fout.close();
	return aux;
}
