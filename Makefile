# Makefile de la práctica 4 de la asignatura de Estructura de Datos
# Autores: Manuel Gachs Ballegeer
# 	   Gonzalo Moreno Soto
# Fecha: Diciembre 2018

####################################################################
####		DECLARACIÓN DE VARIABLES Y DIRECTIVAS		####
####################################################################

# Compilador y flags
CXX := g++
CXXFLAGS := -g

# Variables de directorios
BIN := bin/
DATA := data/
DOC := doc/
INC := include/
OBJ := obj/
SRC := src/
TESTS := test/

# Variables de archivos
# Diccionario
DIC := diccionario
DIC_EXEC := $(BIN)$(DIC)
DIC_HDR := $(DIC).cpp $(DIC).h
DIC_SRC := uso$(DIC).cpp
DIC_OBJ = $(DIC_SRC:.cpp=.o)
DIC_TEST := $(DATA)$(DIC)1.test $(DATA)$(DIC)2.test
# Guía telefónica
TLF := guiatlf
TLF_EXEC := $(BIN)$(TLF)
TLF_HDR := $(TLF).cpp $(TLF).h
TLF_SRC := uso$(TLF).cpp
TLF_OBJ = $(TLF_SRC:.cpp=.o)
TLF_TEST := $(DATA)$(TLF).test

# Variables de reglas
define create_obj =
@echo "Compilando $@ ..."
@$(CXX) $(CXXFLAGS) -o $@ $<
@echo "${GREEN}Hecho${RESET}"
endef
define create_bin =
@echo "Creando el archivo ejecutable $@ ..."
@$(CXX) $(CXXFLAGS) -o $@ $^
@echo "${GREEN}Hecho${RESET}"
@mv $^ $(OBJ)
endef
define make_dir =
@echo "Creando directorio $@ ..."
@mkdir -p $@
@echo "${GREEN}Hecho${RESET}"
endef
define perform_test =
@echo "${BOLD}Realizando test para $<:${RESET}"
@./$< $(TEST)
endef

# Otras variables
TEST :=
DOCUMENTATION := doxygen
BROWSER := firefox
GREEN := `tput setaf 2`
BOLD := `tput bold`
RESET := `tput sgr0`

# Variables específicas a un patrón o regla
%.o: CXXFLAGS += -Wall -I./$(INC) -I./$(SRC) -c
dic_test: TEST += $(DIC_TEST)
tlf_test: TEST += $(TLF_TEST)

# Directivas
vpath %.h $(INC)
vpath %.cpp $(SRC)
vpath %.test $(DATA)

####################################################################
####				REGLAS				####
####################################################################

# Reglas sin receta
all: $(DIC_EXEC) $(TLF_EXEC) help author
dic: $(DIC_EXEC)
tlf: $(TLF_EXEC)
.PHONY: clean $(DOCUMENTATION)
$(DIC_EXEC): | $(BIN)
$(TLF_EXEC): | $(BIN)
$(DIC_OBJ): | $(OBJ)
$(TLF_OBJ): | $(OBJ)
dic_test: | $(TESTS)
tlf_test: | $(TESTS)

# Diccionario
$(DIC_EXEC): $(DIC_OBJ)
	$(create_bin)

$(DIC_OBJ): $(DIC_SRC) $(DIC_HDR)
	$(create_obj)

# Guía telefónica
$(TLF_EXEC): $(TLF_OBJ)
	$(create_bin)

$(TLF_OBJ): $(TLF_SRC) $(TLF_HDR)
	$(create_obj)

# Generales
$(BIN):
	$(make_dir)

$(OBJ):
	$(make_dir)

$(TESTS):
	$(make_dir)

clean:
	@-rm -rf $(BIN) $(OBJ) $(TESTS)
	@-rm -rf $(DOC)/html $(DOC)/latex

dic_test: $(DIC_EXEC)
	$(perform_test)

tlf_test: $(TLF_EXEC)
	$(perform_test)

$(DOCUMENTATION):
	@-rm -rf $(DOC)/html $(DOC)/latex
	@$(DOCUMENTATION) $(DOC)p4.doxy
	@$(BROWSER) $(DOC)/html/index.html &

help:
	@echo "OPCIONES:"
	@echo "	${BOLD}author			\
	${RESET}Muestra información del autor del proyecto"
	@echo "	${BOLD}clean			\
	${RESET}Elimina los archivos objeto, ejecutables y tests"
	@echo "	${BOLD}dic			\
	${RESET}Crea el ejecutable de ejemplo de la clase diccionario"
	@echo "	${BOLD}dic_test			\
	${RESET}Ejecuta el test de la clase diccionario"
	@echo "	${BOLD}doxygen [BROWSER=]	\
	${RESET}Muestra la documentación en BROWSER (firefox por defecto)"
	@echo "	${BOLD}help			\
	${RESET}Muestra este mensaje de ayuda"
	@echo "	${BOLD}tlf			\
	${RESET}Crea el ejecutable de ejemplo de la clase guía telefónica"
	@echo "	${BOLD}tlf_test			\
	${RESET}Ejecuta el test de la clase guía telefónica"

author:
	@echo "Este proyecto ha sido realizado por:"
	@echo "-Manuel Gachs Ballegeer: \
	https://gitlab.com/Manuelbelgicano"
	@echo "-Gonzalo Moreno Soto: \
	https://gitlab.com/delightfulagony"
	@echo "Para la asignatura de Estructura de Datos de la UGR"
