/**
 * @file diccionario.cpp
 * @authors Profesorado UGR
 * 	    Manuel Gachs Ballegeer
 * 	    Gonzalo Moreno Soto
 */
#include<iostream>

template <class T, class U>
bool operator<(const data<T,U> &d1,const data <T,U>&d2) {
	if (d1.clave<d2.clave)
		return true;
	else
		return false;
}

template <class T, class U>
void diccionario<T,U>::copiar(const diccionario<T,U>& D) {
	datos.assign(D.datos.begin(),D.datos.end());
}

template <class T, class U>
void diccionario<T,U>::borrar() {
	this->datos.erase(datos.begin(),datos.end());
}

template <class T, class U>
diccionario<T,U>& diccionario<T,U>::operator=(const diccionario<T,U> &D) {
	if (this!=&D){
		borrar();
		copiar(D);
	}
	return *this;
}

template <class T, class U>
diccionario<T,U>& diccionario<T,U>::operator+(const diccionario<T,U> &d) {
	const_iterator it;
	iterator clave_pos;
	typename std::list<U>::iterator it_d;
	typename std::list<U>::iterator it_this; 
	T _clave;
	std::list<U> _info;
	bool nueva = true;

	for (it=d.cbegin();it!=d.cend();it++) {
		_clave = (*it).clave;
		_info = (*it).info_asoci;

		if(!estaClave(_clave,clave_pos))
			insertar(_clave,_info);
		else {
			for (it_d=_info.begin();it_d!=_info.end();it_d++) {
				nueva = true;
				for (it_this=(*clave_pos).info_asoci.begin();it_this!=(*clave_pos).info_asoci.end() || !nueva;it_this++)
					if ((*it_this)==(*it_d))
						nueva = false;
				if (nueva)
					(*clave_pos).info_asoci.push_back((*it_d));
			}
		}

	}
	return *this;
}

template <class T, class U>
void diccionario<T,U>::erase(const T &p,const U &s) {
	iterator it_out;
	if (estaClave(p,it_out)) {
		typename std::list<U>::iterator it;
		for (it=(*it_out).info_asoci.begin();it!=(*it_out).info_asoci.end();it++)
			if (*it==s) {
				(*it_out).info_asoci.erase(it);
				return;
			}
	}
	return;
}

template <class T, class U>
bool diccionario<T,U>::estaClave(const T &p,typename std::list<data<T,U> >::iterator &it_out) {
	if (datos.size()>0) {
		iterator it;
		for (it=begin(); it!=end() ;++it) {
			if ((*it).clave==p) {
				it_out=it;
				return true;
			} else if ((*it).clave>p) {
				it_out=it;
				return false;
			}	  
		}
		it_out=it;
		return false;
	} else {	
		it_out=datos.end();
		return false;
	}	    
}

template <class T, class U>
void diccionario<T,U>::insertar(const T &clave,const std::list<U> &info) {
	iterator it;
	if (!estaClave(clave,it)) {
		data<T,U> p;
		p.clave = clave;
		p.info_asoci=info;
		datos.insert(it,p);
	}
}

template <class T, class U>
void diccionario<T,U>::addSignificadoPalabra(const T &p,const U &s) {
	iterator it;
	if (!estaClave(p,it)) {
		data<T,U> nuevaPalabra;
		nuevaPalabra.clave = p;
		nuevaPalabra.info_asoci.push_back(s);
		datos.push_back(nuevaPalabra);
		datos.sort();
	}
	(*it).info_asoci.push_back(s);
}

template <class T, class U>
std::list<U> diccionario<T,U>::getInfoAsoc(const T & p) const {
	diccionario<T,U> aux = *this;
	iterator it;
	if (!aux.estaClave(p,it)) {
		return std::list<U>();
	} else {
		return (*it).info_asoci;
	}
}

template <class T, class U>
T diccionario<T,U>::getClave(const size_t &i) const {
	const_iterator it = cbegin();
	size_t posicion = 0;
	while (posicion<=i && it!=datos.end()) {
		posicion++;
		++it;
	}
	data<T,U> d = *it;
	return d.clave;
}
