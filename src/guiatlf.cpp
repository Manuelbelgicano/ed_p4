istream & operator>>(istream &is,pair<std::string,std::string> &d){ 
	getline(is,d.first,'\t');
	getline(is,d.second);
	return is;
}
		    
std::string  Guia_Tlf::gettelefono(const std::string & nombre){
	map<std::string,std::string>::iterator it=datos.find(nombre);
	if (it==datos.end()) 
		return std::string("");
	else return it->second;
}

pair<map<std::string,std::string>::iterator,bool>  Guia_Tlf::insert(std::string nombre, std::string tlf){
	pair<std::string,std::string> p (nombre,tlf);
	pair< map<std::string,std::string> ::iterator,bool> ret;
	ret=datos.insert(p); 
	return ret;     
}
		    
pair<map<std::string,std::string>::iterator,bool>  Guia_Tlf::insert(pair<std::string,std::string> p){
	pair<map<std::string,std::string> ::iterator,bool> ret;
	ret=datos.insert(p); 
	return ret;     
}

void Guia_Tlf::borrar(const std::string &nombre){
	map<std::string,std::string>::iterator itlow = datos.lower_bound(nombre);//el primero que tiene dicho nombre
	map<std::string,std::string>::iterator itupper = datos.upper_bound(nombre);//el primero que ya no tiene dicho nombre
	datos.erase(itlow,itupper);//borramos todos aquellos que tiene dicho nombre
}

void Guia_Tlf::borrar(const std::string &nombre,const std::string &tlf){
	map<std::string,std::string>::iterator itlow = datos.lower_bound(nombre);//el primero que tiene dicho nombre
	map<std::string,std::string>::iterator itupper = datos.upper_bound(nombre);//el primero que ya no tiene dicho nombre
	map<std::string,std::string>::iterator it;
	bool salir =false;
	for (it=itlow; it!=itupper && !salir;++it){
		if (it->second==tlf){
			datos.erase(it);
			salir =true;
		}		
	}		
}

Guia_Tlf Guia_Tlf::operator+(const Guia_Tlf & g){
	Guia_Tlf aux(*this);
	map<std::string,std::string>::const_iterator it;
	for (it=g.datos.begin();it!=g.datos.end();++it){
		aux.insert(it->first,it->second);
	}	  
	return aux;
}	   
		    
Guia_Tlf Guia_Tlf::operator-(const Guia_Tlf & g){
	Guia_Tlf aux(*this);
	map<std::string,std::string>::const_iterator it;
	for (it=g.datos.begin();it!=g.datos.end();++it){
		aux.borrar(it->first,it->second);
	}	  
	return aux;
}
		    
ostream & operator<<(ostream & os, Guia_Tlf & g){
	map<std::string,std::string>::iterator it;
		for (it=g.datos.begin(); it!=g.datos.end();++it){
		os<<it->first<<"\t"<<it->second<<endl;
		}
	return os;
}
		    
istream & operator>>(istream & is, Guia_Tlf & g){
	pair<std::string,std::string> p;
	Guia_Tlf aux;
	while (is>>p){
		aux.insert(p);
	}
	g=aux;
	return is;
}

bool Guia_Tlf::cambiartelefono(const std::string &nombre, const std::string &telefono) {
	map<std::string,std::string>::iterator it=datos.find(nombre);
	if(it==datos.end())
		return false;
	else {
		pair<std::string,std::string> dupla(nombre,telefono);
		datos.erase(it);
		datos.insert(dupla);
		return true;
	}
}

bool Guia_Tlf::cambiarnombre(const std::string &nombreantiguo, const std::string &nombrenuevo) {
	map<std::string,std::string>::iterator it=datos.find(nombreantiguo);
	if(it==datos.end())
		return false;
	else {
		std::string telefono = datos[nombreantiguo];
		pair<std::string,std::string> dupla(nombrenuevo,telefono);
		datos.erase(it);
		datos.insert(dupla);
		return true;
	}
}

Guia_Tlf Guia_Tlf::operator+=(const Guia_Tlf & g) {
	*this=*this+g;
	return *this;
}
