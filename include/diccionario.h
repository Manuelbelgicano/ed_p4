/**
 * @file diccionario.h
 * @authors Profesorado UGR
 *          Manuel Gachs Ballegeer
 *          Gonzalo Moreno Soto
 */
#include <list>

#ifndef _DICCIONARIO_H
#define _DICCIONARIO_H

/**
 * Tipo elemento que define el diccionario. T es el tipo de dato asociado a una clave que no se 
 * repite (DNI p.ej.) y std::list<U> es una std::lista de datos (string p.ej) asociados a la clave
 * de tipo T. El diccionario está ordenado de menor a mayor clave.
 */
template <class T,class U>
struct data {
    T clave;
    std::list<U> info_asoci;
 };
 
/**
 * @brief Ordena 2 registros de acuerdo a la clave de tipo T
 * @param d1 Primer registro que se compara
 * @param d2 Segundo registro que se compara
 * @return @retval true si el primero es menor que el segundo
 * 	   @retval false si el primero no es menor que el segundo
 */
template <class T,class U>
bool operator<(const data<T,U> &d1,const data<T,U> &d2);

/**
 * TDA Diccionario
 */
template <class T,class U>
class diccionario{
private:	  
	std::list<data<T,U> > datos;
	void copiar(const diccionario<T,U>& D);
	void borrar();
public:
	/**
	 * @brief Constructor por defecto
	 */
	diccionario():datos(std::list<data<T,U> >()) {}
	/**
	 * @brief Constructor por copia
	 * @param D Diccionario que se va a copiar
	 */
	diccionario(const diccionario &D) {copiar(D);}
	/**
	 * @brief Destructor
	 */
	~diccionario() {}
	/**
	 * @brief Sobrecarga del operador de asignación
	 * @param D Diccionario que se va a asignar
	 * @return El diccionario asignado
	 */
	diccionario<T,U>& operator=(const diccionario<T,U> &D);
	/**
	 * @brief Realiza la unión de dos diccionarios
	 * @param d Diccionario que se une
	 * @return La unión de los diccionarios
	 */
	diccionario<T,U>& operator+(const diccionario<T,U> &d);
	/**
	 * @brief Elimina un significado a una palabra
	 * @param p Clave de la palabra
	 * @param s Significado que se quiere borrar
	 */
	void erase(const T &p,const U &s);
	/**
	 * @brief Busca una clave en el diccionario
	 * @param p Clave que se busca
	 * @param it_out Iterador de la clase. Es modificado
	 * @return @retval true si encuentra la clave
	 * 	   @retval flase si no encuentra la clave
	 * @post Si encuentra la clave el iterador es un iterador a la clave buscada. En caso
	 * 	 contrario se convierte en un iterador al final del diccionario.
	 */
	bool estaClave(const T &p,typename std::list<data<T,U> >::iterator &it_out);
	/**
	 * @brief Inserta un nuevo registro en el diccionario
	 * @param clave Clave del nuevo registro
	 * @param info Información del nuevo registro
	 * @post Si la clave ya existía, no hace nada
	 */
	void insertar(const T &clave,const std::list<U> &info);	 
	/**
	 * @brief Añade una información a una clave
	 * @param s Nueva información
	 * @param p Clave a la que se le añade la información
	 * @post Si la clave no existía anteriormente, la añade con la información correspondiente
	 */
	void addSignificadoPalabra(const T &p,const U &s);
	/**
	 * @brief Elimina un registro del diccionario
	 * @param p Clave del registro que se quiere borrar
	 */
	void erase(const T &p);
	/**
	 * @brief Devuelve la información asociada a una clave
	 * @param p Clave de la que se quiere saber la información
	 * @return La información asociada a la clave
	 */
	std::list<U> getInfoAsoc(const T &p) const;
	/**
	 * @brief Devuelve el tamaño del diccionario
	 * @return El tamaño del diccionario
	 */
	size_t size() const {return datos.size();}
	/**
	 * @brief Devuelve una clave del diccionario
	 * @param i La posición de la clave que se busca
	 * @return La clave buscada
	 */
	T getClave(const size_t &i) const;

	typedef typename std::list<data<T,U> >::iterator iterator;
	typedef typename std::list<data<T,U> >::const_iterator const_iterator;

	/**
	 * @brief Devuelve un iterador al inicio del diccionario
	 * @return Un iterador al inicio del diccionario
	 */
	iterator begin() {return datos.begin();}
	/**
	 * @brief Devuelve un iterador al final del diccionario
	 * @return Un iterador al final del diccionario
	 */
	iterator end() {return datos.end();}
	/**
	 * @brief Devuelve un iterador constante al inicio del diccionario
	 * @return Un iterador constante al inicio del diccionario
	 */
	const_iterator cbegin() const {return datos.begin();}
	/**
	 * @brief Devuelve un iterador constante al final del diccionario
	 * @return Un iterador constante al final del diccionario
	 */
	const_iterator cend() const {return datos.end();}
};

#include"diccionario.cpp"
#endif
